openteacher (3.2-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:12:44 -0500

openteacher (3.2-2) unstable; urgency=medium

  * Added debian/patches:
    - 06_install_ubuntu_html.patch.
    - 07_using_openteacher_2_html.patch.
    - 08_base_html.patch.
    - 09_download_html.patch.
    - 10_index_html.patch
    - This corrects the lintian errors privacy-breach-generic,
      privacy-breach-google-adsense, privacy-breach-facebook as reported on
      l-d-o.
  * Removed websiteGenerator/scripts/jquery-1.9.1.min.js, during clean and
    symlink to system lib. Closes: #737447
  * Increased Standard Version to 3.9.5 no changes needed.

 -- Charlie Smotherman <cjsmo@cableone.net>  Thu, 06 Feb 2014 02:48:43 -0500

openteacher (3.2-1) unstable; urgency=low

  [ Charlie Smotherman ]
  * New upstream release.
  * Update packaging to use pybuild.
  * Update manpage
  * Added debian/clean to exclude courtousy copies of code, and extra license
    files.
  * debian/rules
    - added rules to install into usr/lib now that OT builds extensions.
  * Update debian/copyright to include new content.
  * Removed debian/docs and debian/doc-base not needed any more.
  * debian/control
    - added B-D on python-dev and dh-python.
    - changed Architecture from all to any.
    - added ${shlibs:Depends}, libjs-jquery-mobile, ibus-qt4 and espeak to
      Depends.
  * Added symlinks for static pics now in /usr/share updated debian/links.
  * Added debian/patches/04desktop to correct Version field typo.
  * Added debian/patches/05setup_py to split out pic files and place them
    into /usr/share.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Charlie Smotherman <cjsmo@cableone.net>  Fri, 02 Aug 2013 18:05:22 +0200

openteacher (2.3-1) unstable; urgency=low

  * New upstream release.

 -- Charlie Smotherman <cjsmo@cableone.net>  Fri, 29 Jun 2012 19:23:43 -0500

openteacher (2.2.1-2) unstable; urgency=low

  * Added B-D on imagemagick for icon resizing.
  * Added B-D on libicu48 to compile icon and gui resources.
  * Added Depends on hicolor-icon-theme.
  * Add icon resizing rules to debian/rules.
  * Install icons into hicolor-icon-theme.
  * Increased Standards version to 3.9.3.
  * Removed debian/openteacher.xpm not needed, it is now generated in
    debian/rules.
  * Update debian/watch so it works again.
  * Added rules to clean *.po files so package builds twice in a row.
    Closes: #671246

 -- Charlie Smotherman <cjsmo@cableone.net>  Mon, 04 May 2012 20:07:23 -0500

openteacher (2.2.1-1) unstable; urgency=low

  * New upstream release.  Upstream has corrected a bug, which when using the
    shuffle mode in openteacher, openteacher would freeze.  This was reported
    directly to upstream via IRC and no bug report has been opened.
  * Corrected typo in debian/control Vcs* field.

 -- Charlie Smotherman <cjsmo@cableone.net>  Sat, 29 Oct 2011 11:57:33 -0500

openteacher (2.2-1) unstable; urgency=low

  * Initial release.  Closes: #608163 (LP: #682852)

 -- Charlie Smotherman <cjsmo@cableone.net>  Tue, 13 Sep 2011 21:18:37 -0500
