# Chinese (Traditional) translation for openteacher
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2012-08-23 23:41+0000\n"
"Last-Translator: Louie Chen <louie23@gmail.com>\n"
"Language-Team: Chinese (Traditional) <zh_TW@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:42+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: plainTextWords.py:65
msgid ""
"Please enter the plain text in the text edit. Separate words with a new line "
"and questions from answers with an equals sign ('=') or a tab."
msgstr ""
"請在文字編輯器內輸入文字。以換行來分開單字並以等號（‘＝’）或 tab 來分隔問題和"
"答案。"

#: plainTextWords.py:66
msgid "Plain text words enterer"
msgstr "文字編輯器"

#: plainTextWords.py:79
msgid "Missing equals sign or tab"
msgstr "缺少 = 或 tab"

#: plainTextWords.py:80
msgid ""
"Please make sure every line contains an '='-sign or tab between the "
"questions and answers."
msgstr "請確定每行在問題和答案間都有包含 = 或 tab"

#: plainTextWords.py:167
msgid "Create words lesson by entering plain text"
msgstr "輸入文字來建立單字課程"
