# Chinese (Traditional) translation for openteacher
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the openteacher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: openteacher\n"
"Report-Msgid-Bugs-To: openteachermaintainers@lists.launchpad.net\n"
"POT-Creation-Date: 2013-06-27 15:37+0200\n"
"PO-Revision-Date: 2012-07-18 09:37+0000\n"
"Last-Translator: Louie Chen <louie23@gmail.com>\n"
"Language-Team: Chinese (Traditional) <zh_TW@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-06-04 05:42+0000\n"
"X-Generator: Launchpad (build 16660)\n"

#: settings.py:109
msgid "Settings"
msgstr "設置"

#: settings.py:110
msgid "Simple mode"
msgstr "簡單模式"

#: settings.py:111
msgid "Advanced mode"
msgstr "進階模式"
